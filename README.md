# Springboot API running on AWS Lambda

## Goals

- Practicing some more Java and SpringBoot
- Play around with Lambdas (or general serverless compute)
- Try out some improvements to my .vimrc for working with Java
- Play around with some DevOps practices for working with AWS again

## TODO

- [x] Java environment setup - Java 17? (play around with Java version management?)
- [x] Springboot initializer?
- [] LSP Setup for Java (write some java code first without it)
- [] Autoformat
- [] Linting
- [] API stuff? (Docs, discoverability, spec?)
- [] Do we containerize for a lambda?
- [] Does it talk to a database?
- [] AWS mocking/local simulator stuff? <https://docs.aws.amazon.com/lambda/latest/dg/images-test.html>
- [] Deploying to AWS
- [] Testing?

## Notes

### Java Version Management Practices

Remember hearing something about just setting JAVA_PATH variable in bashrc.
Pretty sure I already do something similar on my work laptop.
<https://stackoverflow.com/a/26252993>

Using sdkman on arch for managing java installs....
<https://sdkman.io/>
Going to avoid using this for now as not sure how wide this is being adopted.

Arch currently has OpenJDK 21 as the default so going to start with this for now.

### Springboot initializer

Lets do it from scratch today.
Start with gradle wrapper. Had gradle installed via pacman.
```
archlinux% pacman -Ss gradle
extra/gradle 8.3-1 [installed]
    Powerful build system for the JVM
extra/gradle-doc 8.3-1
    Powerful build system for the JVM (documentation)
extra/gradle-src 8.3-1
    Powerful build system for the JVM (sources)
```

`gradle init` seems to work

Is it correct practice to check in the gradle folder with the gradle-wrapper.jar?

Turns out Spring needs the Application class to be in a package (other than the
default unamed one) for @ComponentScan to work.

### Java dev environment thoughts

#### Shell completion for gradle?

<https://github.com/gradle/gradle-completion>
Think I played around with this for a few minutes and was horrified at how slow
it was! Will leave for now.
