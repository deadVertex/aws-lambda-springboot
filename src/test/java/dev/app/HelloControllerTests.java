package dev.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class HelloControllerTests {
    @Test
    void hello() {
        HelloController controller = new HelloController();
        assertEquals("Hello", controller.hello());
    }
}
